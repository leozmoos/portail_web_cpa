<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Fill Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>CPA Fill form</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>CPA Fill form</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Breadcrumb Section End -->
<!-- CPA Form Start-->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contact V9</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" method="post" action="index.php?action=roomsState">
            <span class="contact100-form-title">
                CPA form
            </span>

            <h2 class="spacingtop">Informations supplémentaires</h2>
            <p>Veuillez remplir tout les champs:</p><br>
            <h3>Chambres:</h3><br>
                <?php


                foreach ($_SESSION['NameRooms'] as $nameRoom)
                {
                    echo '<table class="spacingbottom">
                <tr>
                    <th class="spacingbottom" style="text-decoration: underline;">'.$nameRoom.'</th>
                    <th class="spacingright">Très mauvais</th>
                    <th class="spacingright ">Mauvais</th>
                    <th class="spacingright ">Moyen</th>
                    <th class="spacingright ">Bon</th>
                    <th class="">Très bon</th>
                </tr>

                
                <tr>
                    <td class="first-col">Lumières</td>
                    <td><input type="radio" value="1" name="Lights'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="2" name="Lights'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="3" name="Lights'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="4" name="Lights'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="5" name="Lights'.$nameRoom.'."required/></td>
                </tr>
                
                <tr>
                    <td class="first-col spacingright">Portes</td>
                    <td><input type="radio" value="1" name="Doors'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="2" name="Doors'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="3" name="Doors'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="4" name="Doors'.$nameRoom.'."required/></td>
                    <td><input type="radio" value="5" name="Doors'.$nameRoom.'."required/></td>
                </tr>
            </table>';
                }
                ?>

                <div class="container-contact100-form-btn spacingleft">
                    <button class="contact100-form-btn" style="color: #000000">Send CPA Form
                    </button>
                </div>
        </form>
    </div>
</div>

<!--===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
