<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Settings Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->
<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>Settings page</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>Settings page</span>
                        <br><br>
                        <table class="spacingtop">
                            <tr>
                               <td class="spacingright spacingbottom"><h3> Account settings</h3></td>
                               <td class="spacingleft spacingbottom"><h3> Mails settings</h3></td>
                            </tr>

                            <tr class="spacingtop spacingbottom">
                                <td class="spacingright">
                                    <div class="container-contact100-form-btn">
                                        <button class="contact100-form-btn">
                                            <a href="index.php?action=chgPwd_View" class="contact-btn-a">
                                                Change your password
                                            </a>
                                        </button>
                                    </div>
                                </td>

                                <td class="spacingleft">
                                    <div class="container-contact100-form-btn">
                                        <button class="contact100-form-btn">
                                            <a href="index.php?action=crtML_View" class="contact-btn-a">
                                                Create mail list
                                            </a>
                                        </button>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="spacingright"></td>
                                <td class="spacingleft">
                                    <div class="container-contact100-form-btn">
                                        <button class="contact100-form-btn">
                                            <a href="index.php?action=shwML_View" class="contact-btn-a">
                                                Show list(s)
                                            </a>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section End -->
<!-- CPA Form Start--




<!-===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
