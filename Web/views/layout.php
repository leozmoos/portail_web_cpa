<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Sona Template">
    <meta name="keywords" content="Sona, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $titre?></title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="img/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Section Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="canvas-open">
    <i class="icon_menu"></i>
</div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="icon_close"></i>
    </div>
    <div class="search-icon  search-switch">
        <i class="icon_search"></i>
    </div>

    <nav class="mainmenu mobile-menu">
        <ul>
            <li class="active"><a href="./index.html">Home</a></li>
            <li><a href="./rooms.html">Rooms</a></li>
            <li><a href="./about-us.html">About Us</a></li>
            <li><a href="./pages.html">Pages</a>
                <ul class="dropdown">
                    <li><a href="./room-details.html">Room Details</a></li>
                    <li><a href="#">Deluxe Room</a></li>
                    <li><a href="#">Family Room</a></li>
                    <li><a href="#">Premium Room</a></li>
                </ul>
            </li>
            <li><a href="./blog.html">News</a></li>
            <li><a href="./contact.html">Contact</a></li>
        </ul>
    </nav>
    <ul class="top-widget">
    </ul>
</div>
<!-- Offcanvas Menu Section End -->

<!-- Header Section Begin -->
<header class="header-section">
    <div class="top-nav">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="tn-left">
                        <li><i class="fa fa-envelope"></i> info.cpa_portal@test.com</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-item">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo">
                        <a href="./index.php">
                            <img src="img/logo.png" alt="">
                        </a>

                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="nav-menu">
                        <nav class="mainmenu">
                            <ul>
                                <li <?php if (isset($_GET['action']) && $_GET['action'] == "home") echo 'class="active"'?>><a href="./index.php?action=home">Home</a></li>
                                <li <?php if (isset($_GET['action']) && $_GET['action'] == "aboutus") echo 'class="active"'?>><a href="./index.php?action=aboutus">About Us</a></li>
                                <!-- CPA FORM / Settings / Statisticals / Shelter lists / Logout-->
                                <?php if (isset($_SESSION['user']))
                                {
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "fillcpa") echo "class='active'"; echo "><a href='./index.php?action=fillcpa'>CPA Form fill</a></li>";
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "settings") echo "class='active'"; echo "><a href='./index.php?action=settings'>Settings</a></li>";
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "stats") echo "class='active'"; echo "><a href='./index.php?action=stats'>Statisticals</a></li>";
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "shelterFilters") echo "class='active'"; echo "><a href='./index.php?action=shelterFilters'>Shelters List</a></li>";
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "createshelter") echo "class='active'"; echo "><a href='./index.php?action=createshelter'>Create shelter</a></li>";
                                    echo "<li "; if (isset($_GET['action']) && $_GET['action'] == "sendmail") echo "class='active'"; echo "><a href='./index.php?action=sendmail_View'>Send Mail</a></li>";
                                }
                                ?>
                                <li <?php if (isset($_GET['action']) && $_GET['action'] == "login") echo 'class="active"'?>><a href="./index.php?action=<?php if (isset($_SESSION['user'])) echo "logout"; else echo "login"?>"><?php if (isset($_SESSION['user'])) echo "Logout"; else echo "Login"?></a></li>
                            </ul>
                        </nav>
                        <div class="nav-right search-switch">
                            <i class="icon_search"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header End -->

<?php echo $contenu?>

<!-- Footer Section Begin -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-text">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ft-about">
                        <div class="logo">
                            <a href="#">
                                <img src="img/Logo_Inverse.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="ft-contact">
                        <h6>Contacter nous</h6>
                        <ul>
                            <li>024 123 45 67</li>
                            <li>info.cpa_portal@test.com</li>
                            <li>Rue de l'Épicéa, Lausanne, Switzerland</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="ft-newslatter">
                        <h6>New latest</h6>
                        <p>Get the latest updates and offers.</p>
                        <form action="#" class="fn-form">
                            <input type="text" placeholder="Email">
                            <button type="submit"><i class="fa fa-send"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <ul>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Terms of use</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Environmental Policy</a></li>
                    </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Search model Begin -->
<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
</body>

</html>