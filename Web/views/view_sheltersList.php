<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Change password Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>Shelters list page</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>Shelters list page</span>
                        <br><br><br>
                        <h3 class="spacingbottom">Veuillez séléctionner un emplacement:</h3>
                        <table class="spacingtop">
                            <tr>
                                <td class="spacingright spacingbottom"><h3> Trier par commune:</h3></td>
                                <td class="spacingleft spacingbottom"><h3> Trier par région:</h3></td>
                            </tr>

                            <tr class="spacingtop spacingbottom">
                                <td class="spacingright">
                                    <div class="container-contact100-form-btn">
                                            <select id="Municipalities">
                                                <?php
                                                foreach ($ListMunicipalities as $element => $element_value)
                                                {
                                                    foreach ($element_value as $municipalitie)
                                                    {
                                                        echo'<option value="'.$municipalitie.'">'.
                                                            $municipalitie.'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                    </div>
                                    <ul id="users" class="spacingtop"></ul>
                                </td>

                                <td class="spacingleft">
                                    <div class="container-contact100-form-btn">
                                        <select id="Regions">
                                            <?php
                                            foreach ($ListRegions as $element => $element_value)
                                            {
                                                foreach ($element_value as $regions)
                                                {
                                                    echo'<option value="'.$regions.'">'.
                                                        $regions.'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </div>
                </div>
                <div class="textareastyle">
                    <ul>

                    <?php
                    if(isset($_GET['municipalitie']))
                    {
                        //temporary array to format the final string
                        foreach ($ListSheltersByMunicipalitie as $element => $element_value)
                        {
                            $temp = array();
                            foreach ($element_value as $Shelter)
                            {
                                array_push($temp,$Shelter);
                            }
                            echo "<li><a href='index.php?action=viewshelter&name=$temp[0]'>- $temp[0] managé par: <b>($temp[1])</b> </li>";
                        }
                    }

                    if(isset($_GET['region']))
                    {
                        //temporary array to format the final string
                        foreach ($ListSheltersByRegion as $element => $element_value)
                        {
                            $temp = array();
                            foreach ($element_value as $Shelter)
                            {
                                array_push($temp,$Shelter);
                            }
                            echo "<li><a href='index.php?action=viewshelter&name=$temp[0]'>- $temp[0] managé par: <b>($temp[1])</b> </li>";
                        }
                    }

                    ?>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var selectMun = document.getElementById('Municipalities');
    var selectReg = document.getElementById('Regions');

    selectReg.onchange = function () {
        var region = selectReg.value;
        document.location = "index.php?action=shelterFilters&region=" + region;
    }

    selectMun.onchange = function () {

        var municipalitie = selectMun.value;
        document.location = "index.php?action=shelterFilters&municipalitie=" + municipalitie;
    }

</script>


<!-===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>

</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
