<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Fill Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>CPA Fill form</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>CPA Fill form</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Breadcrumb Section End -->
<!-- CPA Form Start-->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contact V9</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" method="post" action="index.php?action=fillcpaform">
            <span class="contact100-form-title">
                CPA form
            </span>

            <h2>Veuillez séléctionner l'abri PC:</h2>
            <p>Si l'abri n'apparaît pas dans la liste, veuillez remplir le champ texte</p><br>
            <select name="ShelterName">
                <?php
                    foreach ($result as $element => $element_value)
                    {
                        foreach ($element_value as $sheltername)
                        {
                            echo'<option value="'.$sheltername.'">'.
                                $sheltername.'</option>';
                        }
                    }
                ?>
            </select>
            <br><br>
            <!--<input type="text" name="sheltername" placeholder="Ex. Abri de l'hôpital">-->
            <br><br>
            <h2 class="spacingtop">Système de ventilation</h2>
            <p>Veuillez remplir tout les champs:</p><br>
            <table>
                <tr>
                    <th class="spacingright">
                        <p>Le tuyau flexible est-il intact ?<br></p>
                        <input type="radio" name="Pipe" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="Pipe" value="0" required>
                        <span class="spacing">Non</span>
                        <span class="focus-input100"></span>
                    </th>

                    <th>
                        <p>Les plombs sont-ils présents ?</p>
                        <input type="radio" name="Fuses" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="Fuses" value="0" required>
                        <span class="spacing">Non</span>
                    </th>
                </tr>
                <tr>
                    <th class="spacingright">
                        <p class="spacingtop">Le filtre à gaz est-il emballé dans du plastique ?<br></p>
                        <input type="radio" name="GazFilter" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="GazFilter" value="0" required>
                        <span class="spacing">Non</span>
                    </th>

                    <th>
                        <p class="spacingtop">Le mode d’emploi est-il à disposition ?</p>
                        <input type="radio" name="Guideline" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="Guideline" value="0" required>
                        <span class="spacing">Non</span>
                    </th>
                </tr>

                <tr>
                    <th class="spacingright">
                        <p class="spacingtop">La manivelle est-elle présente ?<br></p>
                        <input type="radio" name="Handwheel" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="Handwheel" value="0" required>
                        <span class="spacing">Non</span>
                    </th>
                </tr>
            </table>

            <h2 class="spacingtop">Portes et volets blindés</h2>
            <p>Veuillez remplir tout les champs:</p>

            <table>
                <tr>
                    <th class="spacingright">
                        <p class="spacingtop">Le dispositif d’auto-libération est-il disponible ?<br></p>
                        <input type="radio" name="SelfReleasing" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="SelfReleasing" value="0">
                        <span class="spacing">Non</span>
                    </th>

                    <th>
                        <p class="spacingtop">En cas de porte blindée sans seuil, le seuil amovible est-il disponible ?</p>
                        <input type="radio" name="Threshold" value="1" checked required>
                        <span class="spacing">Oui</span>

                        <input type="radio" name="Threshold" value="0" required>
                        <span class="spacing">Non</span>
                    </th>
                </tr>
            </table>

            <h2 class="spacingtop">État de l'abri PC</h2>
            <p>Veuillez remplir tout les champs:</p><br>

            <table>
                <tr>
                    <th class="first-col"></th>
                    <th class="spacingright">Très Mauvais</th>
                    <th class="spacingright">Mauvais</th>
                    <th class="spacingright">Moyen</th>
                    <th class="spacingright">Bon</th>
                    <th>Très bon</th>
                </tr>
                <tr>
                    <td class="first-col spacingright">Hall</td>
                    <td><input type="radio" value="1" name="HallState" required/></td>
                    <td><input type="radio" value="2" name="HallState" required/></td>
                    <td><input type="radio" value="3" name="HallState" required/></td>
                    <td><input type="radio" value="3" name="HallState" required/></td>
                    <td><input type="radio" value="3" name="HallState" required/></td>
                </tr>
                <tr>
                    <td class="first-col spacingright">Cuisine</td>
                    <td><input type="radio" value="1" name="KitchenState" required/></td>
                    <td><input type="radio" value="2" name="KitchenState" required/></td>
                    <td><input type="radio" value="3" name="KitchenState" required/></td>
                    <td><input type="radio" value="4" name="KitchenState" required/></td>
                    <td><input type="radio" value="5" name="KitchenState" required/></td>
                </tr>
                <tr>
                    <td class="first-col">Portes</td>
                    <td><input type="radio" value="1" name="DoorsState" required/></td>
                    <td><input type="radio" value="2" name="DoorsState" required/></td>
                    <td><input type="radio" value="3" name="DoorsState" required/></td>
                    <td><input type="radio" value="4" name="DoorsState" required/></td>
                    <td><input type="radio" value="5" name="DoorsState" required/></td>
                </tr>
                <tr>
                    <td class="first-col">Lumières</td>
                    <td><input type="radio" value="1" name="LightsState" required/></td>
                    <td><input type="radio" value="2" name="LightsState" required/></td>
                    <td><input type="radio" value="3" name="LightsState" required/></td>
                    <td><input type="radio" value="4" name="LightsState" required/></td>
                    <td><input type="radio" value="5" name="LightsState" required/></td>
                </tr>
            </table>
            <h2 class="spacingtop">Remarques personalisées</h2>
            <p>Veuillez remplir ce champ:</p>
            <div class="wrap-input100 validate-input spacingtop" data-validate = "Please enter your message">
                <textarea class="input100" name="message" placeholder="Your Message" required></textarea>
                <span class="focus-input100"></span>
            </div>

            <div class="container-contact100-form-btn spacingleft">
                <button class="contact100-form-btn" style="color: #000000">Send CPA Form
                </button>
            </div>
        </form>
    </div>
</div>
<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
