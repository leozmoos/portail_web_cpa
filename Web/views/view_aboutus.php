<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - About US";
// ouvre la mémoire tampon
ob_start();

?>

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>About Us</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>About Us</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section End -->

<!-- About Us Page Section Begin -->
<section class="aboutus-page-section spad">
    <div class="container">
        <div class="about-page-text">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ap-title">
                        <h2>Bienvenue sur le portail Web CPA</h2>
                        <p>Ce site web a été conçu pour simplifier la gestion des contrôles périodiques des abris (CPA).</p><p>L'objectif est également de mettre en lumière ce travail effectué par des personnes de la protection civile</p>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <ul class="ap-services">
                        <li><i class="icon_check"></i> Contrôle périodique des abris PC</li>
                        <li><i class="icon_check"></i> Réparations mineures sur site</li>
                        <li><i class="icon_check"></i> Nettoyage et entretien</li>
                        <li><i class="icon_check"></i> Saisie des défauts & transfert des données</li>
                        <li><i class="icon_check"></i> Entretien final avec les autorités responsables</li>
                        <li><i class="icon_check"></i> Contrôle de suivi</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-page-services">
            <div class="row">
                <div class="col-md-4">
                    <div class="ap-service-item set-bg" data-setbg="img/services/ProtectionCivile.jpg">
                        <div class="api-text">
                            <h3>Protection civile</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ap-service-item set-bg" data-setbg="img/services/ProtectionCivile2.jpg">
                        <div class="api-text">
                            <h3>Rénovation & Entretien</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ap-service-item set-bg" data-setbg="img/services/ProtectionCivile3.jpg">
                        <div class="api-text">
                            <h3>Saisie des données & transfert aux autorités</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Page Section End -->

<!-- Video Section Begin -->
<section class="video-section set-bg" data-setbg="img/bg-01-Blur.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="video-text">
                    <h2>Découvrer nos différents services en vidéo</h2>
                    <p>Cette vidéo est un exemple</p>
                    <a href="https://www.youtube.com/watch?v=Dd6h-IQdhDI" class="play-btn video-popup"><img
                                src="img/play.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Video Section End -->

<!-- Gallery Section Begin -->
<section class="gallery-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span>Notre Gallerie</span>
                    <h2>Découvrer notre travail</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="gallery-item set-bg" data-setbg="img/services/ProtectionCivile4.jpg">
                    <div class="gi-text">
                        <h3>Réparations sur site</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="gallery-item set-bg" data-setbg="img/services/ProtectionCivile5.jpg">
                            <div class="gi-text">
                                <h3>Entretien</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="gallery-item set-bg" data-setbg="img/services/ProtectionCivile7.jpg">
                            <div class="gi-text">
                                <h4><font color="white">Saisie des données</font></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gallery-item large-item set-bg" data-setbg="img/services/ProtectionCivile6.jpg">
                    <div class="gi-text">
                        <h3>Contrôle et rénovation</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Gallery Section End -->

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
