<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Fill Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>New shelter form</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>New shelter form</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section End -->
<!-- CPA Form Start-->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contact V9</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" method="post" action="index.php?action=newSheltertoDB">
            <span class="contact100-form-title">
                New shelter form
            </span>
            <h3>Chambres:</h3><br>
            <p>Veuillez remplir tout les champs:</p><br>
                <?php
                if(isset($_GET['error']) && $_GET['error'] == "roomsnameexisting") $error = "error";

                for($i = 0; $i < $_SESSION['nbRooms'];$i++)
                {
                    echo "<table class='spacingbottom'>
                    <p>Nom de la chambre:</p><br>
                    <input type='text' placeholder='Ex. Rossa' name='Room$i' class='$error' value='".$_SESSION["RoomsData"][0]["Room$i"]."' required>
                        <tr>
                            <th class='spacingright'></th>
                            <th class='spacingright'>Très mauvais</th>
                            <th class='spacingright'>Mauvais</th>
                            <th class='spacingright'>Moyen</th>
                            <th class='spacingright'>Bon</th>
                            <th class=''>Très bon</th>
                        </tr>
            
                    <tr>
                    <td class='first-col'>Lumières</td>";
                    for ($j=1;$j<=5;$j++)
                    {
                        echo "<td><input type='radio' value='$j' name='Lights$i'".($_SESSION["RoomsData"][0]["Lights$i"]==$j ? "checked" : " ")." required/></td>";
                    }

                    echo "</tr><tr><td class='first-col spacingright'>Portes</td>";

                    for ($j=1;$j<=5;$j++)
                    {
                        echo "<td><input type='radio' value='$j' name='Doors$i'".($_SESSION["RoomsData"][0]["Doors$i"]==$j ? "checked" : " ")." required/></td>";
                    }
                    echo "</tr></table>";
                }
                ?>

                <div class="container-contact100-form-btn spacingleft">
                    <button class="contact100-form-btn" style="color: #000000">Send new shelter Form
                    </button>
                </div>
        </form>
    </div>
</div>


<!--===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
