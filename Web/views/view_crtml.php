<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - Create mail list Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->
<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>Create mail list page</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>Create mail list page</span>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb Section End -->
<!-- Create mail list form-->
<div class="container-contact100">
    <div class="wrap-contact100">

            <span class="contact100-form-title">
                Create mail list
            </span>
            <h2 class="spacingtop" style="text-decoration: underline">Veuillez entrer le nom de la liste:</h2><br>
            <?php if(isset($_GET['error']) && $_GET['error'] == "mailexisting")
            {
                echo "<p style=\"color: #f72200;\">Le nom est déjà existant</p>";
                echo "<input class=\"error\" type=\"text\" id=\"mlname\" placeholder=\"Ex. Section Lausannoise\" class=\"spacingbottom\" required>";
            }
            else {
                echo "<input type=\"text\" id=\"mlname\" placeholder=\"Ex. Section Lausannoise\" class=\"spacingbottom\" required>";
            }
            ?>

            <form id="myform" class="spacingtop" onsubmit="return addUser()">
                <h2 style="text-decoration: underline">Ajouter un utilisateur:</h2>
                <input class="spacingtop" id="email" type="email" name="email" placeholder="Adresse email" required/>
                <button type="submit" class="spacingbottom">Ajouter</button>
            </form>

            <h2 class="spacingtop" style="text-decoration: underline">Liste des utilisateurs:</h2>
            <ul id="users" class="spacingtop"></ul>

            <form method="post" action="index.php?action=createML">
                <div class="container-contact100-form-btn spacingleft">
                    <button type="submit" class="contact100-form-btn" style="color: #000000">
                        <input id="hiddenDatas" name="datas" value="" hidden>
                        <input id="hiddenName" name="name" value="" hidden>
                            Create mail list
                    </button>
                </div>
            </form>
        </div>
    </div>

<script>
    var items = [];
    var list = document.getElementById('users');
    var name = document.getElementById("mlname").value;

    function addUser(){
        var email = document.getElementById('email').value;
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode("-" + email));
        list.appendChild(entry);

        items.push(email);

        document.cookie="profile_viewer_uid=1";
        document.getElementById("hiddenDatas").value = items;
        document.getElementById("hiddenName").value = document.getElementById("mlname").value;

        console.log(items);
        return false;
    }
</script>


<!-===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
