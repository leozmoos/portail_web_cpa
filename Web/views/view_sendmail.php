<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Settings Page";
// ouvre la mémoire tampon
ob_start();

?>

<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->
<!-- Breadcrumb Section Begin -->
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" method="post" action="index.php?action=sendCPAmail">
            <span class="contact100-form-title">
                Envoyer le rapport CPA par mail
            </span>
            <h3>Liste de distribution:</h3><br>
            <p>Veuillez séléctionner la liste de distribution:</p><br>
            <table class="spacingbottom">
                <tr>
                    <td class="first-col">
                        <select name="MailList">
                            <?php
                            foreach ($datasML as $element => $element_value)
                            {
                                echo'<option value="'.$element_value.'">'.$element_value.'</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <h3>Rapport CPA à envoyer:</h3><br>
            <p>Veuillez séléctionner la rapport CPA à envoyer:</p><br>
            <table class="spacingbottom">
                <tr>
                    <td>
                        <select name="CPA">
                            <?php
                            foreach ($datasCPA as $element => $element_value)
                            {
                                echo'<option value="'.$datasShelters[$element].'">'.$element_value.'</option>';
                            }

                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <h2 class="spacingtop">Messages personalisées:</h2>
            <p>Veuillez remplir ce champ:</p>
            <div class="wrap-input100 validate-input spacingtop" data-validate = "Please enter your message">
                <textarea class="input100" name="message" placeholder="Votre Message" required></textarea>
                <span class="focus-input100"></span>
            </div>

            <div class="container-contact100-form-btn spacingleft">
                <button class="contact100-form-btn" style="color: #000000">Envoyer le rapport CPA par mail
                </button>
            </div>
        </form>
    </div>
</div>
<!-- Breadcrumb Section End -->
<!-- CPA Form Start--

<!-===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
