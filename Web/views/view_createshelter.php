<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Fill Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->

<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>Create a new shelter</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>Create a new shelter</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Breadcrumb Section End -->
<!-- CPA Form Start-->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contact V9</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" method="post" action="index.php?action=newshelter">
            <span class="contact100-form-title">
                New shelter form
            </span>

            <h3>Veuillez indiquer le nom de l'abri PC:</h3>
            <p>Veuillez remplir tout les champs:</p>
            <?php if(isset($_GET['error']) && $_GET['error'] == "shelternameexisting")
            {
                echo "<p style=\"color: #f72200;\">Le nom est déjà existant</p>";
                echo "<input class=\"error\" type=\"text\" placeholder=\"Ex.Abri Rosia\" class=\"spacingtop\" name=\"sheltername\" required>";
            }
            else {
                echo "<input type=\"text\" placeholder=\"Ex.Abri Rosia\" class=\"spacingtop\" name=\"sheltername\" required>";
            }
            ?>

            <br><br>

            <table>
                <tr>
                    <td class="spacingtop spacingright">
                        <p class="spacingtop">Veuillez saisir l'emplacement: </p>
                        <html>
                        <head>
                            <style>
                                input {
                                    height: 30px;
                                    padding-left: 10px;
                                    border-radius: 4px;
                                    border: 1px solid rgb(186, 178, 178);
                                    box-shadow: 0px 0px 12px #EFEFEF;
                                }
                            </style>
                            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA08iNXPF_vTgVbzD8gsdfkwy54X7k7Kxg&libraries=places"></script>
                        </head>
                        <body>
                        <input type="text" id="autocomplete" name="municipalities" value="<?php if (isset($_SESSION['NewShelterData'][0]['municipalities'])) echo $_SESSION['NewShelterData'][0]['municipalities'];?>" required/>

                        <script>
                            var input = document.getElementById('autocomplete');
                            var autocomplete = new google.maps.places.Autocomplete(input,{types: ['(cities)']});
                            autocomplete.setComponentRestrictions({'country': ['ch']});
                            google.maps.event.addListener(autocomplete, 'place_changed', function(){
                                var place = autocomplete.getPlace();
                            })
                        </script>
                        </body>
                        </html>
                    </td>

                    <td class="spacingtop">
                        <p class="spacingtop">Veuillez saisir la région: </p>
                        <select id="Region" name="Region">
                            <option value="Lemanique">Région lémanique</option>
                            <option value="Mittelland">Espace Mittelland</option>
                            <option value="SNO">Suisse du Nord-Ouest</option>
                            <option value="ZRH">Zurich</option>
                            <option value="SO">Suisse orientale</option>
                            <option value="SC">Suisse centrale</option>
                            <option value="Tessin">Tessin</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <h3 class="spacingtop">Informations administratives:</h3>
                    <p>Veuillez remplir tout les champs:</p>
                    <td class="spacingtop spacingright">
                        <p class="spacingtop">Veuillez saisir le nombre de chambres:</p>
                        <input type="number" placeholder="Ex. 2" name="nbRooms" value="<?php if (isset($_SESSION['NewShelterData'][0]['nbRooms'])) echo $_SESSION['NewShelterData'][0]['nbRooms'];?>" required>
                    </td>
                    <td class="spacingtop">
                        <p class="spacingtop">Veuillez saisir le nom du manager:</p>
                        <input type="text" placeholder="Ex. Michel Durat" name="managerName" value="<?php if (isset($_SESSION['NewShelterData'][0]['managerName'])) echo $_SESSION['NewShelterData'][0]['managerName'];?>" required>
                    </td>
                </tr>

            </table><br>

            <h3 class="spacingtop">État de l'abri PC:</h3>
            <p>Veuillez remplir tout les champs:</p><br>

            <table>
                <tr>
                    <th class="first-col"></th>
                    <th class="spacingright">Très Mauvais</th>
                    <th class="spacingright">Mauvais</th>
                    <th class="spacingright">Moyen</th>
                    <th class="spacingright">Bon</th>
                    <th>Très bon</th>
                </tr>
                <tr>
                    <td class="first-col spacingright">Hall</td>
                    <?php
                    // Create Hall State Buttons with prefilled value if available
                    for ($j=1;$j<=5;$j++)
                    {
                        echo "<td><input type='radio' value='$j' name='HallState'".($_SESSION['NewShelterData'][0]['HallState']==$j ? "checked" : " ").' required/></td>';
                    }
                    ?>
                </tr>
                <tr>
                    <td class="first-col spacingright">Cuisine</td>
                    <?php
                    // Create Kitchen State Buttons with prefilled value if available
                    for ($j=1;$j<=5;$j++)
                    {
                        echo "<td><input type='radio' value='$j' name='KitchenState'".($_SESSION['NewShelterData'][0]['KitchenState']==$j ? "checked" : " ").' required/></td>';
                    }
                    ?>
                </tr>
                <tr>
                    <td class="first-col">Portes</td>
                    <?php
                    // Create Doors State Buttons with prefilled value if available
                    for ($j = 1; $j <= 5; $j++) {
                        echo "<td><input type='radio' value='$j' name='DoorsState'".($_SESSION['NewShelterData'][0]['DoorsState'] == $j ? "checked" : " ").' required/></td>';
                    }
                    ?>
                </tr>
                <tr>
                    <td class="first-col">Lumières</td>
                    <?php
                    // Create Light State Buttons with prefilled value if available
                    for ($j = 1; $j <= 5; $j++) {
                        echo "<td><input type='radio' value='$j' name='LightsState'".($_SESSION['NewShelterData'][0]['LightsState'] == $j ? "checked" : " ").' required/></td>';
                    }
                    ?>
                </tr>
            </table>


            <div class="container-contact100-form-btn spacingleft">
                <button class="contact100-form-btn" style="color: #000000">Send new Shelter Form
                </button>
            </div>
        </form>
    </div>
</div>


<!--===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>



</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
