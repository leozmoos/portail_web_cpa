<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - CPA Change password Page";
// ouvre la mémoire tampon
ob_start();

?>
<!-===============================================================================================-->
<link rel="icon" type="image/png" href="../CPAForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="../CPAForm/css/util.css">
<link rel="stylesheet" type="text/css" href="../CPAForm/css/main.css">
<!--===============================================================================================-->
<!-- Breadcrumb Section Begin -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text">
                    <h2>Statisticals page</h2>
                    <div class="bt-option">
                        <a href="./index.php?action=home">Home</a>
                        <span>Statisticals page</span>
                        <br><br><br>

                        <?php

                        $dataPoints1 = array(
                            array("label"=> "Très Bon", "y"=> $vgoodStateArrayCount),
                            array("label"=> "Bon", "y"=> $goodStateArrayCount),
                            array("label"=> "Moyen", "y"=> $averageStateArrayCount),
                            array("label"=> "Mauvais", "y"=> $badStateArrayCount),
                            array("label"=> "Très mauvais", "y"=> $vbadStateArrayCount),
                        );


                        ?>
                        <div id="chartContainer" style="height: 370px; width: 100%;" class="spacingbottom"></div><br><br>
                        <!--<div id="chartContainer2" style="height: 370px; width: 100%;"></div>-->
                        <table>
                            <tr>
                                <td>
                                    <select id="ShelterName">
                                        <?php
                                        foreach ($AllShelterName as $element => $element_value)
                                        {
                                            foreach ($element_value as $sheltername)
                                            {
                                                echo'<option value="'.$sheltername.'">'.
                                                    $sheltername.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="Year">
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                    </select>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <div id="chartContainer2" style="height: 370px; width: 100%;" class="spacingtop"></div>

                        <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var select = document.getElementById('ShelterName');
    var selectYear = document.getElementById('Year');

    select.onchange = function () {
        var value = this.value;
        var year = selectYear.value;

        document.location = "index.php?action=stats&shelter=" + value + "&year=" + year;
    }

    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            exportEnabled: true,
            title: {
                text: "Etat général des abris"
            },
            subtitles: [{
                text: "Depuis l'année 2020"
            }],
            data: [{
                type: "pie",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - #percent%",
                yValueFormatString: "#,##0",
                dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartContainer2", {
            animationEnabled: true,
            title:{
                text: "Rapport de l'état général de l'abri: <?php if (isset($_GET['shelter'])) echo $_GET['shelter']?>"
            },
            axisX:{
                valueFormatString: "DD MMM",
                crosshair: {
                    enabled: true,
                    snapToDataPoint: true
                }
            },
            axisY: {
                title: "Etat général de l'abri",
                includeZero: false,
                valueFormatString: "##0.00",
                crosshair: {
                    enabled: true,
                    snapToDataPoint: true,
                    labelFormatter: function(e) {
                        return CanvasJS.formatNumber(e.value, "##0.00");
                    }
                }
            },
            data: [{
                type: "area",
                xValueFormatString: "MMM",
                yValueFormatString: "$##0.00",
                dataPoints: [
                    { x: new Date('2016-07-01'), y: 5},
                    { x: new Date('2016-09-01'), y: 4},
                    { x: new Date('2016-11-01'), y: 2},
                    { x: new Date('2016-12-01'), y: 3},
                    ]
                }]
            });

            chart.render();

        var chart3 = new CanvasJS.Chart("chartContainer3", {
            animationEnabled: true,
            title: {
                text: "Rapport de l'état général de l'abri: <?php if (isset($_GET['shelter'])) echo $_GET['shelter']?>"
            },
            axisX: {
                title: "Année: <?php if (isset($_GET['year'])) echo $_GET['year']?>"
            },
            axisY: {
                title: "Percentage",
                suffix: "%"
            },
            data: [{
                type: "line",
                name: "CPU Utilization",
                connectNullData: true,
                //nullDataLineDashType: "solid",
                xValueType: "string",
                xValueFormatString: "YY",
                yValueFormatString: "#,##0.##\"%\"",
                dataPoints: [
                    { x: "Janvier", y: 35.939 },
                    { x: 1501052273000, y: 40.896 },
                ]
            }]
        });
        chart3.render();
    }
</script>

<!-===============================================================================================-->
<script src="../CPAForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/bootstrap/js/popper.js"></script>
<script src="../CPAForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/daterangepicker/moment.min.js"></script>
<script src="../CPAForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/map-custom.js"></script>
<!--===============================================================================================-->
<script src="../CPAForm/js/main.js"></script>

</body>
</html>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
