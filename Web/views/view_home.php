<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "CPA Web Portal - Home";
// ouvre la mémoire tampon
ob_start();

?>

<!-- About Us Section Begin -->
<section class="aboutus-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-text">
                    <div class="section-title">
                        <span>Bienvenue sur le</span>
                        <h2>Portail Web<br />CPA </h2>
                    </div>
                    <p class="f-para"><title>Buts du contrôle périodique des abris CPA</title>

                        Le contrôle périodique des abris permet:<br>

                        <p>- de connaître l’état de préparation technique des abris et d’en fournir une vue d’ensemble à la Confédération,
                        aux cantons et aux communes;</p>
                    <p>- de détecter les défauts et les besoins de modernisation;</p>
                    <p>- de faire comprendre aux propriétaires d’abris l’utilité d’un entretien adapté.</p>
                    <a href="index.php?action=aboutus" class="primary-btn about-btn">En savoir plus</a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-pic">
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="img/shelters/shelter1.jpg" alt="">
                        </div>
                        <div class="col-sm-6">
                            <img src="img/shelters/shelter2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Section End -->

<!-- Services Section End -->
<section class="services-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span>Que faisons-nous?</span>
                    <h2>Découvrer nos Services</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-036-parking"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Contrôle périodique des abris PC</a></h4>
                    <p>La loi fédérale impose aux autorités d'inspecter tous les abris au moins tous les 10 ans, de dresser la liste des défauts et de les faire éliminer...</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-020-telephone"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Réparations mineures sur site</a></h4>
                    <p>Tous les défauts mineurs et les travaux d'entretien ou les travaux de maintenance rapides sont effectuées par nos contrôleurs lors du CPA. </p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-043-room-service"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Nettoyage et entretien</a></h4>
                    <p>Les différents composants essentiels au bon fonctionnement des abris PC sont nettoyés et entretenus régulièrement</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-010-newspaper"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Saisie des défauts et transfert des données aux autorités</a></h4>
                    <p> Les défauts identifiés sont saisis et envoyés aux autorités sous la forme d'un rapport accompagné de photos...
                    </p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-050-bellboy"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Entretien final avec les autorités responsables</a></h4>
                    <p>Les évaluations de l'état de préparation des abris, ainsi que les défauts constatés, sont discutées et analysées avec les autorités responsables.</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="service-item">
                    <i class="flaticon-044-clock-1"></i>
                    <h4><a href="index.php?action=aboutus" class="linkcolor">Contrôle de suivi</a></h4>
                    <p>A l'expiration du délai fixé pour l'élimination des défauts, pour remédier aux manquements, le contrôle de suivi a lieu conformément aux instructions des autorités.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->

<!-- Home Room Section Begin -->
<section class="hp-room-section">
    <div class="container-fluid">
        <div class="hp-room-items">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="hp-room-item set-bg" data-setbg="img/shelters/shelter3.jpg">
                        <div class="hr-text">
                            <h2>Vue intérieure</h2>
                            <h3>Orbe - <span>Abri PC</span></h3>
                            <a href="index.php?action=aboutus" class="primary-btn">En savoir plus</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="hp-room-item set-bg" data-setbg="img/shelters/shelter4.jpg">
                        <div class="hr-text">
                            <h2>Vue intérieure</h2>
                            <h3>Cran Montana - <span>Abri PC</span></h3>
                            <a href="index.php?action=aboutus" class="primary-btn">En savoir plus</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="hp-room-item set-bg" data-setbg="img/shelters/shelter5.jpg">
                        <div class="hr-text">
                            <h2>Vue intérieure</h2>
                            <h3>Lucens<span> - Abri PC</span></h3>
                            <a href="index.php?action=aboutus" class="primary-btn">En savoir plus</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="hp-room-item set-bg" data-setbg="img/shelters/shelter6.jpg">
                        <div class="hr-text">
                            <h2>Vue extérieure</h2>
                            <h3>Sion <span>- Abri PC Hôpital</span></h3>
                            <a href="index.php?action=aboutus" class="primary-btn">En savoir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Home Room Section End -->

<!-- Blog Section Begin -->
<section class="blog-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span>Nos différents abris</span>
                    <h2>Photos et informations</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="blog-item set-bg" data-setbg="img/shelters/shelter7.jpg">
                    <div class="bi-text">
                        <span class="b-tag">Abri PC - Intérieur</span>
                        <h4><a href="index.php?action=aboutus">Cheseaux-sur-Lausanne</a></h4>
                        <div class="b-time"><i class="icon_clock_alt"></i> 15 Novembre, 2019</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-item set-bg" data-setbg="img/shelters/shelter8.jpg">
                    <div class="bi-text">
                        <span class="b-tag">Abri PC - Intérieur</span>
                        <h4><a href="index.php?action=aboutus">Veysonnaz</a></h4>
                        <div class="b-time"><i class="icon_clock_alt"></i> 25 Août, 2019</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-item set-bg" data-setbg="img/shelters/shelter9.jpg">
                    <div class="bi-text">
                        <span class="b-tag">Abri PC - Extérieur</span>
                        <h4><a href="index.php?action=aboutus">Crans</a></h4>
                        <div class="b-time"><i class="icon_clock_alt"></i> 10 Mai, 2019</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="blog-item small-size set-bg" data-setbg="img/shelters/shelter10.jpg">
                    <div class="bi-text">
                        <span class="b-tag">Abri PC - Intérieur</span>
                        <h4><a href="index.php?action=aboutus">Estavayer-le-lac</a></h4>
                        <div class="b-time"><i class="icon_clock_alt"></i> 02 Mars, 2020</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-item small-size set-bg" data-setbg="img/shelters/shelter11.jpg">
                    <div class="bi-text">
                        <span class="b-tag">Abri PC - Intérieur</span>
                        <h4><a href="index.php?action=aboutus">Payerne</a></h4>
                        <div class="b-time"><i class="icon_clock_alt"></i> 12 Avril, 2019</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
