<?php
session_start();
/**
 * User: Leo.ZMOOS
 * Index.php : page de triage des actions reçues dans l'URL
 */

require "controler/controler.php";

try
{
    if (isset($_GET['action']))
    {
        $action = $_GET['action'];
        // Sélection de l'action passée par l'URL
        switch ($action)
        {
            case 'home': home(); break;

            case 'aboutus': aboutus(); break;

            case 'login': login(); break;

            case 'fillcpa': fillCPA_View(); break;

            case 'signin': signin(); break;

            case 'logout': logout(); break;

            case 'fillcpaform': fillCPADatas(); break;

            case 'settings': settingsView(); break;

            case 'chgPwd_View': chgPwd_View(); break;

            case 'chgPwd': chgPwd(); break;

            case 'crtML_View': crtML_View(); break;

            case 'shwML_View': shwML_View(); break;

            case 'chgPwd_View_Success': chgPwd_View_Success(); break;

            case 'stats': Statisticals_View(); break;

            case 'roomsState': roomsState(); break;

            case 'createshelter': createshelter_View(); break;

            case 'newshelter': newshelter(); break;

            case 'newSheltertoDB': newSheltertoDB(); break;

            case 'createML': createML(); break;

            case 'sendmail_View': sendmail_View(); break;

            case 'sendCPAmail': sendCPAmail(); break;

            case 'shelterFilters': shelterFilters(); break;

            case 'viewshelter': viewshelter(); break;


            default: throw new Exception("Action non valide");
        }
    }
    else
        home();
}
catch (Exception $e)
{
    //erreur($e->getMessage());
}